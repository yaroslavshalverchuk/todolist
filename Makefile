start:
	docker-compose up -d
	docker-compose exec php composer install
	docker-compose exec php php bin/console doctrine:migrations:migrate

down:
	docker-compose down

restart:
	docker-compose down
	docker-compose up -d
	docker-compose exec php composer install
	docker-compose exec php bin/console doctrine:migrations:migrate

build:
	cp .env.example .env
	docker-compose up -d --build
	docker-compose exec php composer install
	docker-compose exec php php bin/console --no-interaction doctrine:migrations:migrate

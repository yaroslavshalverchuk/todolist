<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220418101552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE task_list (id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, created_at INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_377B6C63A76ED395 ON task_list (user_id)');
        $this->addSql('CREATE TABLE task_list_item (id INT NOT NULL, task_list_id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(1023) DEFAULT NULL, status SMALLINT NOT NULL, created_at INT NOT NULL, updated_at INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_25FF37A6224F3C61 ON task_list_item (task_list_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, created_at INT DEFAULT NULL, updated_at INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE task_list ADD CONSTRAINT FK_377B6C63A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task_list_item ADD CONSTRAINT FK_25FF37A6224F3C61 FOREIGN KEY (task_list_id) REFERENCES task_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task_list_item DROP CONSTRAINT FK_25FF37A6224F3C61');
        $this->addSql('ALTER TABLE task_list DROP CONSTRAINT FK_377B6C63A76ED395');
        $this->addSql('DROP TABLE task_list');
        $this->addSql('DROP TABLE task_list_item');
        $this->addSql('DROP TABLE "user"');
    }
}

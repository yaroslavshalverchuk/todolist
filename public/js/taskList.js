class TaskList {

    constructor() {
        this.inputs = document.querySelectorAll('.task-list-item-checkbox input');
        this.init();
    }

    init = function () {
        let self = this;
        this.inputs.forEach(input => input.addEventListener("change", function (event) {
            let checked = event.target.checked === true ? 1 : 0;
            let id = event.target.dataset.id;
            let token = event.target.dataset.token;
            self.postRequest('/task-list-item/' + id + '/change-status', {
                'status' : checked,
                '_token' : token,
            }, function (data) {
                console.log(data);
            });
        }))
    }

    postRequest = function (url, data, success) {
        var params = Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }).join('&');

        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhr.open('POST', url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState > 3 && xhr.status === 200) {
                success(xhr.responseText);
            }
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        return xhr;
    }
}

window.onload = function (event) {
    var taskList = new TaskList();
};
## 1. Initialization by `make` app
To auto initialize project you need `make` app and `docker-compose`  on your computer. 
### Initialization
Run commands in the project folder
- make build

### Start initialized project
Run commands in the project folder
- make start

### Stop initialized project
Run commands in the project folder
- make down

### Restart initialized project
Run commands in the project folder
- make restart


## 2. Run project manual
To auto initialize project you need `docker-compose` app  on your computer.
### Initialization
Run commands in the project folder
- cp .env.example .env
- docker-compose up -d --build
- docker-compose exec php composer install
- docker-compose exec php php bin/console --no-interaction doctrine:migrations:migrate

### Start initialized project
Run commands in the project folder
- docker-compose up -d
- docker-compose exec php composer install
- docker-compose exec php php bin/console doctrine:migrations:migrate
- docker-compose exec php php bin/console doctrine:fixtures:load

### Stop initialized project
Run commands in the project folder
- docker-compose down

### Restart initialized project
Run commands in the project folder
- docker-compose exec php composer install
- docker-compose exec php bin/console doctrine:migrations:migrate first
- docker-compose down
- docker-compose up -d
- docker-compose exec php bin/console doctrine:migrations:migrate
- docker-compose exec php bin/console doctrine:fixtures:load


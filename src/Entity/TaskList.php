<?php

namespace App\Entity;

use App\Repository\TaskListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=TaskListRepository::class)
 */
class TaskList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="yes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=TaskListItem::class, mappedBy="task_list", orphanRemoval=true)
     */
    private $taskListItems;

    public function __construct()
    {
        $this->taskListItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return (bool) $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->created_at;
    }

    public function setCreatedAt(int $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection<int, TaskListItem>
     */
    public function getTaskListItems(): Collection
    {
        return $this->taskListItems;
    }

    public function addTaskListItem(TaskListItem $taskListItem): self
    {
        if (!$this->taskListItems->contains($taskListItem)) {
            $this->taskListItems[] = $taskListItem;
            $taskListItem->setTaskListId($this);
        }

        return $this;
    }

    public function removeTaskListItem(TaskListItem $taskListItem): self
    {
        if ($this->taskListItems->removeElement($taskListItem)) {
            // set the owning side to null (unless already changed)
            if ($taskListItem->getTaskListId() === $this) {
                $taskListItem->setTaskListId(null);
            }
        }

        return $this;
    }
}

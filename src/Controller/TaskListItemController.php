<?php

namespace App\Controller;

use App\Entity\TaskList;
use App\Entity\TaskListItem;
use App\Form\TaskListItemType;
use App\Repository\TaskListItemRepository;
use App\Repository\TaskListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task-list-item")
 */
class TaskListItemController extends AbstractController
{
    /**
     * @Route("/new/{id}", name="app_task_list_item_new", methods={"GET", "POST"})
     */
    public function new(
        Request $request,
        TaskListItemRepository $taskListItemRepository,
        ?TaskList $taskList
    ): Response
    {
        if ($taskList === null) {
            throw new HttpException(404, 'Task list not found');
        }

        $taskListItem = new TaskListItem();
        $taskListItem->setTaskList($taskList);
        $form = $this->createForm(TaskListItemType::class, $taskListItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskListItemRepository->add($taskListItem);
            return $this->redirectToRoute(
                'app_task_list_show',
                ['id' => $taskList->getId()],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('task_list_item/new.html.twig', [
            'task_list_item' => $taskListItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_task_list_item_show", methods={"POST"})
     */
    public function changeStatus(TaskListItem $taskListItem)
    {

    }

    /**
     * @Route("/{id}/change-status", name="app_task_list_item_show", methods={"POST"})
     * @param Request $request
     * @param TaskListItem $taskListItem
     * @param TaskListItemRepository $taskListItemRepository
     * @param TaskListRepository $taskListRepository
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function show(
        Request $request,
        TaskListItem $taskListItem,
        TaskListItemRepository $taskListItemRepository,
        TaskListRepository $taskListRepository
    ): Response
    {
        $status = (int) $request->get('status');
        if ($this->isCsrfTokenValid($taskListItem->getId() . 'change-status', $request->request->get('_token'))) {
            $taskListItemRepository->changeStatus($taskListItem, $status);
        } else {
            return $this->json(['success' =>  false]);
        }

        $taskListStatus = $taskListItemRepository->isAllCompleted($taskListItem);
        $taskListRepository->changeStatus($taskListItem->getTaskList(), $taskListStatus);

        return $this->json(['success' =>  true]);
    }

    /**
     * @Route("/{id}/edit", name="app_task_list_item_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ?TaskListItem $taskListItem, TaskListItemRepository $taskListItemRepository): Response
    {
        if ($taskListItem === null) {
            throw new HttpException(404, 'Task list item not found');
        }

        $form = $this->createForm(TaskListItemType::class, $taskListItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskListItemRepository->add($taskListItem);
            return $this->redirectToRoute(
                'app_task_list_show',
                ['id' => $taskListItem->getTaskList()->getId()],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('task_list_item/edit.html.twig', [
            'task_list_item' => $taskListItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_task_list_item_delete", methods={"POST"})
     */
    public function delete(
        Request $request,
        ?TaskListItem $taskListItem,
        TaskListItemRepository $taskListItemRepository
    ): Response
    {
        if ($taskListItem === null) {
            throw new HttpException(404, 'Task list item not found');
        }

        $taskListId = $taskListItem->getTaskList()->getId();
        if ($this->isCsrfTokenValid('delete'.$taskListItem->getId(), $request->request->get('_token'))) {
            $taskListItemRepository->remove($taskListItem);
        }

        return $this->redirectToRoute(
            'app_task_list_show',
            ['id' => $taskListId],
            Response::HTTP_SEE_OTHER
        );
    }
}

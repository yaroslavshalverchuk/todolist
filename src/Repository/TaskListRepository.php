<?php

namespace App\Repository;

use App\Entity\TaskList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * @method TaskList|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskList|null findOneBy(array $criteria, array $orderBy = null)]
 * @method TaskList[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskListRepository extends ServiceEntityRepository
{
    /**
     * @var Security
     */
    private $security;


    /**
     * @return TaskList[]|array|object[]
     */
    public function findAll()
    {
        $user = $this->security->getUser();
        return $this->findBy(['user' => $user], ['id' => 'desc']);
    }

    /**
     * @param ManagerRegistry $registry
     * @param Security $security
     */
    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, TaskList::class);
        $this->security = $security;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(TaskList $entity, bool $flush = true): void
    {
        $entity->setCreatedAt(time());
        $entity->setUser($this->security->getUser());
        $entity->setStatus(0);
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(TaskList $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param TaskList $entity
     * @param int $status
     * @param bool $flush
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function changeStatus(TaskList $entity, int $status, bool $flush = true): void
    {
        $entity->setStatus($status);
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}

<?php

namespace App\Repository;

use App\Entity\TaskListItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaskListItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskListItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskListItem[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskListItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskListItem::class);
    }

    /**
     * @return TaskListItem[]|array|object[]
     */
    public function findAll()
    {
        return $this->findBy([], ['id' => 'desc']);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(TaskListItem $entity, bool $flush = true): void
    {
        $entity->setCreatedAt(time());
        $entity->setUpdatedAt(time());
        $entity->setStatus(0);
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(TaskListItem $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param TaskListItem $entity
     * @param int $status
     * @param bool $flush
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function changeStatus(TaskListItem $entity, int $status, bool $flush = true): void
    {
        $entity->setUpdatedAt(time());
        $entity->setStatus($status);
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param TaskListItem $taskListItem
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isAllCompleted(TaskListItem $taskListItem): bool
    {
        $notCompleteTotal = (int) $this->createQueryBuilder('t')
            ->select('count(t.id) as total')
            ->andWhere('t.status = :status AND t.task_list=:task_list_id')
            ->setParameter('status', TaskListItem::NOT_COMPLETE)
            ->setParameter('task_list_id', $taskListItem->getTaskList()->getId())
            ->getQuery()
            ->getSingleScalarResult();

        return $notCompleteTotal === 0;
    }
}
